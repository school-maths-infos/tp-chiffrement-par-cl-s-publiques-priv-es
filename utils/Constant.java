package utils;

import java.math.BigInteger;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Constant {

    public static BigInteger TWO = BigInteger.ONE.add(BigInteger.ONE);
    public static BigInteger MINUS_ONE = new BigInteger("-1");
    public static int NB_PRIME_TEST = 2;
    public static String CHARSET_UTF8 = "UTF-8";
}
