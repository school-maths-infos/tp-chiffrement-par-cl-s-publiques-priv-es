import controllers.Rsa;
import models.Cipher;
import models.Message;

import java.io.File;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Dechiffre {

    public static void main(String args[]) {

        if (args.length == 1) {
            File pubFile = new File(args[0] + ".pub");
            File privFile = new File(args[0] + ".priv");
            if (pubFile.exists() && !pubFile.isDirectory() &&
                    privFile.exists() && !privFile.isDirectory()) {

                Rsa rsa = new Rsa(args[0], args[0]);

                Scanner reader = new Scanner(System.in);
                String s;
                Cipher cipher = new Cipher();
                while (!(s = reader.nextLine()).equals("")) {
                    cipher.add(new BigInteger(s));
                }
                Message message = rsa.decrypt(cipher);
                if (message != null) {
                    System.out.println(message.getMessageText());
                }
                else {
                    System.out.println("Error, the message couldn't be decrypted !");
                }
            } else {
                System.out.println("The files " + args[0] + ".pub and " + args[0] + ".priv" + " must exist.");
            }
        } else {
            System.out.println("Usage : " +
                    "    java Dechiffre [nom]                      : chiffrer le message sur l'entrée standard"
            );
        }
    }
}
