package models;

import utils.Constant;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Message {

    private String messageText;
    private byte[] digest;

    /**
     * Constructor Message
     *
     * @param bytes: Plain text message represented by a byte array
     */
    public Message(byte[] bytes) {
        this.messageText = toMessageText(bytes, 0, bytes.length, Charset.forName(Constant.CHARSET_UTF8));
        this.digest = this.getHash();
    }

    /**
     * Constructor Message
     *
     * @param bytes: Plain text message represented by a byte array
     * @param off: The index of the first byte to decode
     * @param len: The number of bytes to decode
     * @param charset: The {@linkplain java.nio.charset.Charset charset} to be used to
     *         decode the {@code bytes}
     */
    public Message(byte[] bytes, int off, int len, Charset charset) {
        this.messageText = toMessageText(bytes, off, len, charset);
        this.digest = this.getHash();
    }

    /**
     * Constructor Message
     *
     * @param messageText: Plain text message representing
     *                   by a String
     */
    public Message(String messageText) {
        this.messageText = messageText;
        this.digest = this.getHash();
    }

    /**
     * Method used to get message as a byte array
     *
     * @return the message transform into a byte array
     */
    public byte[] getBytes() {
        return this.messageText.getBytes(Charset.forName(Constant.CHARSET_UTF8));
    }

    /**
     * Method to transform a byte array to String
     *
     * @param bytes: The byte array to transform
     * @param off: The index of the first byte to decode
     * @param len: The number of bytes to decode
     * @param charset: The {@linkplain java.nio.charset.Charset charset} to be used to
     *         decode the {@code bytes}
     * @return String plain text
     */
    public String toMessageText(byte[] bytes, int off, int len, Charset charset) {
        return new String(bytes, off, len, charset);
    }

    /**
     * Method to transform a sequence of bytes
     * into a BigInteger
     *
     * @param sequence: plain text to transform
     * @return BigInteger plain text
     */
    public BigInteger sequenceToBigInteger(byte[] sequence) {
        return new BigInteger(1, sequence);
    }

    /**
     * Method to generate a hash
     *
     * @return array of byte md5 hash
     */
    public byte[] getHash() {
        byte[] digest = null;
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(this.messageText.getBytes(Charset.forName("UTF-8")));
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return digest;
    }

    /**
     * Method to display the hash
     * in hexadecimal
     */
    public void displayHash() {
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(String.format("%02x", b & 0xff));
        }

        System.out.println(sb.toString());
    }

    /**
     * Method to transform the hash
     *
     * @return BigInteger hash
     */
    public BigInteger digestToBigInteger() {
        return new BigInteger(this.digest);
    }

    public String getMessageText() {
        return messageText;
    }
}
