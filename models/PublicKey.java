package models;

import java.math.BigInteger;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class PublicKey {

    private int module;
    private BigInteger n;
    private BigInteger b;

    /**
     * Constructor PublicKey
     */
    public PublicKey() {}

    /**
     * Constructor PublicKey
     *
     * @param module: number of bits
     * @param b: a*b = 1 [(p-1)*(q-1)]
     * @param n: multiply number of two prime numbers p, q
     */
    public PublicKey(int module, BigInteger b, BigInteger n) {
        this.module = module;
        this.n = n;
        this.b = b;
    }

    /**
     * Method to display public
     * key attributes
     */
    public void display() {
        System.out.println("Public key:");
        System.out.println("t = " + this.module);
        System.out.println("n = " + this.n);
        System.out.println("b = " + this.b);
    }

    public BigInteger getN() {
        return n;
    }

    public void setN(BigInteger n) {
        this.n = n;
    }

    public BigInteger getB() {
        return b;
    }

    public void setB(BigInteger b) {
        this.b = b;
    }

    public int getModule() {
        return module;
    }

    public void setModule(int module) {
        this.module = module;
    }
}
