package models;

import java.math.BigInteger;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class SecretKey {

    private int module;
    private BigInteger p;
    private BigInteger q;
    private BigInteger a;

    public SecretKey() {}

    /**
     * Constructor SecretKey
     *
     * @param module: number of bits
     * @param a: modular inverse of b if and only if pgcd(b, phiN) = 1
     * @param p: prime number
     * @param q: prime number
     */
    public SecretKey(int module, BigInteger a, BigInteger p, BigInteger q) {
        this.module = module;
        this.a = a;
        this.p = p;
        this.q = q;
    }

    /**
     * Method to display secret
     * key attributes
     */
    public void display() {
        System.out.println("Secret key:");
        System.out.println("t = " + this.module);
        System.out.println("a = " + this.a);
        System.out.println("p = " + this.p);
        System.out.println("q = " + this.q);
    }

    public BigInteger getPhiN() {
        return this.p.subtract(BigInteger.ONE).multiply(this.q.subtract(BigInteger.ONE));
    }

    public BigInteger getP() {
        return p;
    }

    public void setP(BigInteger p) {
        this.p = p;
    }

    public BigInteger getQ() {
        return q;
    }

    public void setQ(BigInteger q) {
        this.q = q;
    }

    public BigInteger getA() {
        return a;
    }

    public void setA(BigInteger a) {
        this.a = a;
    }

    public int getModule() {
        return module;
    }

    public void setModule(int module) {
        this.module = module;
    }

    public BigInteger getN() {
        return this.p.multiply(this.q);
    }
}
