package models;

import java.math.BigInteger;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Sign {

    private BigInteger signText;

    /**
     * Constructor Sign
     *
     * @param signText: plain text signed represented by
     *                a BigInteger
     */
    public Sign(BigInteger signText) {
        this.signText = signText;
    }

    public BigInteger getSignText() {
        return signText;
    }
}
