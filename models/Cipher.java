package models;

import java.math.BigInteger;
import java.util.LinkedList;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Cipher {

    private LinkedList<BigInteger> segmentedCipher;

    /**
     * Constructor Cipher
     */
    public Cipher() {
        this.segmentedCipher = new LinkedList<BigInteger>();
    }

    /**
     * Constructor Cipher
     * @param segmentedCipher: cipher text segmented by
     *                       block into module size@param segmentedCipher
     */
    public Cipher(LinkedList<BigInteger> segmentedCipher) {
        this.segmentedCipher = segmentedCipher;
    }

    /**
     * Method to concatenate segmented encrypt message chunks
     * @param segmentedCipher: cipher text segmented by
     *                       block into module size@param segmentedCipher
     */
    public void add(BigInteger segmentedCipher) {
        this.segmentedCipher.add(segmentedCipher);
    }

    /**
     * Method to display the cipher
     * text by block
     */
    public String display() {
        String cipher = "";
        for(int i = 0; i < this.segmentedCipher.size(); i++) {
            cipher += this.segmentedCipher.get(i) + "\n";
        }
        return cipher;
    }

    public LinkedList<BigInteger> getSegmentedCipher() {
        return segmentedCipher;
    }
}
