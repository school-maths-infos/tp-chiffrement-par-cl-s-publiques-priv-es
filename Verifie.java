import controllers.Rsa;
import models.Message;
import models.Sign;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigInteger;
import java.util.Scanner;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Verifie {
    public static Rsa rsa;

    public static void main(String[] args) {
        if (args.length == 2) {
            String name = args[0];
            File pubFile = new File(name + ".pub");
            File privFile = new File(name + ".priv");
            File signature = new File(args[1]);
            if (pubFile.exists() && !pubFile.isDirectory() &&
                    privFile.exists() && !privFile.isDirectory() &&
                    signature.exists() && !signature.isDirectory()) {
                rsa = new Rsa(name, name);
                Scanner scanner = new Scanner(System.in);
                String messageString = "";
                String line = scanner.nextLine();
                while (scanner.hasNext() && !line.equals("")) {
                    messageString += line;
                    line = scanner.nextLine();
                }
                messageString += line;
                Message message = new Message(messageString);
                try {
                    String content = new Scanner(signature).next();
                    BigInteger s = new BigInteger(content);
                    Sign sign = new Sign(s);
                    boolean isCorrect = rsa.verify(message, sign);
                    System.out.println("- The signature is:\n" + isCorrect);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else {
                System.out.println("The file doesn't exists.");
            }
        } else {
            System.out.println("Usage : " +
                    "    java Verifie [nom] [fichier-signature]   : vérifie la signature du message sur l'entrée standard"
            );
        }
    }
}
