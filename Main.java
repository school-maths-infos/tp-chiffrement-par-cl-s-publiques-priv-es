import controllers.Rsa;
import models.Cipher;
import models.Message;
import models.Sign;
import utils.Constant;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Main {
    public static Rsa rsa;
    private static Scanner reader;
    private static BufferedReader stdin;

    public static void displayMenu() {
        boolean exit = false;
        while (!exit) {
            System.out.println("\n\nChoose a number:");
            System.out.println("0) Exit");
            System.out.println("1) Generate keys");
            System.out.println("2) Generate keys with BigInteger methods");
            System.out.println("3) Display keys");
            System.out.println("4) Encrypt message");
            System.out.println("5) Decrypt message");
            System.out.println("6) Sign message");
            System.out.println("7) Verify signature");
            String line = reader.nextLine();
            try {
                int number = Integer.parseInt(line);
                if (number == 0) {
                    exit = true;
                } else if (number > 0 && number <= 7) {
                    doRsaActionByChoice(number);
                } else {
                    System.out.println("This choice doesn't exists.");
                }
            } catch (NumberFormatException e) {
                System.out.println("It's not a number.");
            }
        }
    }

    public static void doRsaActionByChoice(int number) {
        boolean useCustomMethods;
        switch (number) {
            case 1:
                useCustomMethods = false;
                doRsaGenKey(useCustomMethods);
                break;
            case 2:
                useCustomMethods = true;
                doRsaGenKey(useCustomMethods);
                break;
            case 3:
                doRsaDisplayKeys();
                break;
            case 4:
                doRsaEncrypt();
                break;
            case 5:
                doRsaDecrypt();
                break;
            case 6:
                doRsaSign();
                break;
            case 7:
                doRsaVerifySign();
                break;
        }
    }

    public static ArrayList<String> getArguments(ArrayList questions) {
        ArrayList<String> answers = new ArrayList<String>();
        for (int i = 0; i < questions.size(); i++) {
            System.out.println(questions.get(i));
            answers.add(reader.nextLine());
        }
        return answers;
    }

    public static ArrayList<String> getMessage(String question) {
        ArrayList<String> answers = new ArrayList<String>();
        System.out.println(question);
        String s;
        while (!(s = reader.nextLine()).equals("")) {
            answers.add(s);
        }
        return answers;
    }

    public static void doRsaGenKey(boolean useCustomMethods) {
        ArrayList<String> questions = new ArrayList<String>();
        questions.add("Enter a module size");
        questions.add("Enter the name of the file where you want to save the keys (without .priv or .pub extension)");
        ArrayList<String> answers = getArguments(questions);
        int module = Integer.parseInt(answers.get(0));
        String name = answers.get(1);
        rsa.genKey(name, module, useCustomMethods);
    }

    public static void doRsaDisplayKeys() {
        ArrayList<String> questions = new ArrayList<String>();
        questions.add("Enter the name of the file wich contains keys (without .priv or .pub extension)");
        ArrayList<String> answers = getArguments(questions);
        String name = answers.get(0);
        File pubFile = new File(name + ".pub");
        File privFile = new File(name + ".priv");
        if (pubFile.exists() && !pubFile.isDirectory() &&
                privFile.exists() && !privFile.isDirectory()) {
            rsa.initializePairKeyService(name, name);
            rsa.displayKeys();
        } else {
            System.out.println("The file doesn't exists.");
        }
    }

    public static void doRsaEncrypt() {
        ArrayList<String> questions = new ArrayList<String>();
        questions.add("Enter the name of the file wich contains keys (without .priv or .pub extension)");
        ArrayList<String> answers = getArguments(questions);
        String name = answers.get(0);
        File f = new File(name + ".pub");
        if (f.exists() && !f.isDirectory()) {
            rsa.initializePairKeyService(name, name);

            System.out.println("Enter message to encrypt:");
            Cipher cipher = new Cipher();
            BufferedInputStream bis = null;

            try {
                bis = new BufferedInputStream(System.in);

                byte[] buffer = new byte[rsa.getModule() / 8];
                int bytesRead = 0;

                // Keep reading from the file while there is any content
                // when the end of the stream has been reached, -1 is returned
                while ((bytesRead = bis.read(buffer)) != -1 && bytesRead > 1) {
                    // Process the chunk of bytes read
                    cipher.add(rsa.encrypt(new Message(buffer, 0, bytesRead, Charset.forName(Constant.CHARSET_UTF8))));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("- The message encrypted is:\n" + cipher.display());
        } else {
            System.out.println("The file doesn't exists.");
        }
    }

    public static Message initializeMessage(ArrayList<String> messageSequences) {
        String messageString = "";
        for (int i = 0; i < messageSequences.size(); i++) {
            messageString += messageSequences.get(i) + "\n";
        }
        return new Message(messageString);
    }

    public static Cipher initializeCipher(ArrayList<String> cSequences) {
        Cipher c = new Cipher();
        for (int i = 0; i < cSequences.size(); i++) {
            c.add(new BigInteger(cSequences.get(i)));
        }
        return c;
    }

    public static void doRsaDecrypt() {
        ArrayList<String> questions = new ArrayList<String>();
        questions.add("Enter the name of the file wich contains keys (without .priv or .pub extension)");
        ArrayList<String> answers = getArguments(questions);
        String name = answers.get(0);
        File pubFile = new File(name + ".pub");
        File privFile = new File(name + ".priv");
        if (pubFile.exists() && !pubFile.isDirectory() &&
                privFile.exists() && !privFile.isDirectory()) {
            rsa.initializePairKeyService(name, name);
            ArrayList<String> cipherSequences = getMessage("Enter message encrypted to decrypt:");
            Message message = rsa.decrypt(initializeCipher(cipherSequences));
            System.out.println("- The message decrypted is:\n" + message.getMessageText());
        } else {
            System.out.println("The file doesn't exists.");
        }
    }

    public static void doRsaSign() {
        ArrayList<String> questions = new ArrayList<String>();
        questions.add("Enter the name of the file wich contains keys (without .priv or .pub extension)");
        ArrayList<String> answers = getArguments(questions);
        String name = answers.get(0);
        File pubFile = new File(name + ".pub");
        File privFile = new File(name + ".priv");
        if (pubFile.exists() && !pubFile.isDirectory() &&
                privFile.exists() && !privFile.isDirectory()) {
            rsa.initializePairKeyService(name, name);
            ArrayList<String> messageSequences = getMessage("Enter message to sign:");
            Message message = initializeMessage(messageSequences);
            Sign sign = rsa.sign(message);
            System.out.print("- Message:\n" + message.getMessageText());
            System.out.println("- Hash:");
            message.displayHash();
            System.out.println("- The signature is:\n" + sign.getSignText());
        } else {
            System.out.println("The file doesn't exists.");
        }
    }

    public static void doRsaVerifySign() {
        ArrayList<String> questions = new ArrayList<String>();
        questions.add("Enter the name of the file wich contains keys (without .priv or .pub extension)");
        questions.add("Enter a signed to verify");
        ArrayList<String> answers = getArguments(questions);
        String name = answers.get(0);
        File pubFile = new File(name + ".pub");
        File privFile = new File(name + ".priv");
        if (pubFile.exists() && !pubFile.isDirectory() &&
                privFile.exists() && !privFile.isDirectory()) {
            rsa.initializePairKeyService(name, name);
            BigInteger s = new BigInteger(answers.get(1));
            Sign sign = new Sign(s);
            ArrayList<String> messageSequences = getMessage("Enter your message:");
            boolean isCorrect = rsa.verify(initializeMessage(messageSequences), sign);
            System.out.println("- The signature is:\n" + isCorrect);
        } else {
            System.out.println("The file doesn't exists.");
        }
    }

    public static void main(String[] args) {
        rsa = new Rsa();
        reader = new Scanner(System.in);
        displayMenu();
    }
}