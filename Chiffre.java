import controllers.Rsa;
import models.Cipher;
import models.Message;
import utils.Constant;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Chiffre {

    public static void main(String args[]) {

        if (args.length == 1) {
            File pubFile = new File(args[0] + ".pub");
            File privFile = new File(args[0] + ".priv");
            if (pubFile.exists() && !pubFile.isDirectory() &&
                    privFile.exists() && !privFile.isDirectory()) {

                Rsa rsa = new Rsa(args[0], args[0]);
                Cipher cipher = new Cipher();
                BufferedInputStream bis = null;

                try {
                    bis = new BufferedInputStream(System.in);

                    byte[] buffer = new byte[rsa.getModule() / 8];
                    int bytesRead = 0;

                    // Keep reading from the file while there is any content
                    // when the end of the stream has been reached, -1 is returned
                    while ((bytesRead = bis.read(buffer)) != -1) {
                        // Process the chunk of bytes read
                        cipher.add(rsa.encrypt(new Message(buffer, 0, bytesRead, Charset.forName(Constant.CHARSET_UTF8))));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        bis.close();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }

                System.out.println(cipher.display());
            } else {
                System.out.println("The files " + args[0] + ".pub and " + args[0] + ".priv" + " must exist.");
            }
        } else {
            System.out.println("Usage : " +
                    "    java Chiffre [nom]                        : chiffrer le message sur l'entrée standard"
            );
        }
    }
}
