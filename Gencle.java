import controllers.Rsa;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Gencle {

    public static void main(String args[]) {
        if (args.length == 2) {
            Rsa rsa = new Rsa();
            rsa.genKey(args[0], Integer.parseInt(args[1]), true);
        } else if (args.length == 3 && args[2].equals("--use-biginteger")){
            Rsa rsa = new Rsa();
            rsa.genKey(args[0], Integer.parseInt(args[1]), false);
        } else {
            System.out.println("Usage : " +
                    "    java Gencle [nom] [t]                      : générer les clés en utilisant nos méthodes" +
                    "    java Gencle [nom] [t] --use-biginteger     : générer les clés en utilisant les méthodes de bigInteger"
            );
        }
    }
}
