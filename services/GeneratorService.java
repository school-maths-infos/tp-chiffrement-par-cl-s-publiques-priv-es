package services;

import models.PublicKey;
import models.SecretKey;
import utils.Constant;

import java.math.BigInteger;
import java.util.Random;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class GeneratorService {

    private final ModularService modularService;
    private Random random;

    public GeneratorService() {
        this.random = new Random();
        this.modularService = new ModularService();
    }

    /**
     * Method to generate public key
     * and secret key saving in files
     * .priv and .pub
     *
     * @param module: the number of bits
     * @param useCustomMethods: to know if we use our custom methods or
     *                        BigInteger methods
     * @return PairKeyService with key set
     */
    public PairKeyService genKey(int module, boolean useCustomMethods) {
        // p and q, two prime numbers
        BigInteger p = BigInteger.ZERO;
        BigInteger q = BigInteger.ZERO;
        long startTimePrimality = System.currentTimeMillis();
        while (p.equals(q)) {
            if (useCustomMethods) {
                p = this.genPrime(module / 2, module / 2 + 16);
                q = this.genPrime(module / 2, module / 2 + 16);
            } else {
                p = new BigInteger(module, Constant.NB_PRIME_TEST / 2, this.random);
                q = new BigInteger(module, 1, this.random);
            }
        }
        long endTimePrimality   = System.currentTimeMillis();
        long totalTimePrimality = endTimePrimality - startTimePrimality;
        System.out.println("Time to test the primality: " + totalTimePrimality + " ms");

        // Euler's totient
        // Is the number of inversible numbers
        // phiN = (p-1) * (q-1)
        BigInteger phiN = p.subtract(BigInteger.valueOf(1)).multiply(q.subtract(BigInteger.valueOf(1)));

        // Public key
        BigInteger n = p.multiply(q);
        // ab = 1 [phiN(N)]
        BigInteger b = this.genB(module, phiN);
        PublicKey pk = new PublicKey(module, b, n);

        // Secret key
        BigInteger a;
        long startTimeEuclide = System.currentTimeMillis();
        if (useCustomMethods) {
            a = this.modularService.modInverse(b, phiN);
        } else {
            a = b.modInverse(phiN);
        }
        long endTimeEuclide   = System.currentTimeMillis();
        long totalTimeEuclide = endTimeEuclide - startTimeEuclide;
        System.out.println("Time to calculate an modular inverse: " + totalTimeEuclide + " ms");

        SecretKey sk = new SecretKey(module, a, p, q);

        return new PairKeyService(pk, sk);
    }

    /**
     * Method to generate a
     * Rules: a*b = 1 [phiN] <=> b has a modular inverse if
     * and only if pgcd(b, phiN) = 1
     *
     * @param module: number of bits
     * @param phiN: (p-1)(q-1)
     * @return b prime with phiN
     */
    public BigInteger genB(int module, BigInteger phiN) {
        BigInteger b = new BigInteger(module, this.random);
        while (!b.gcd(phiN).equals(BigInteger.ONE)) {
            b = new BigInteger(module, this.random);
        }
        return b;
    }

    /**
     * Method to generate prime number
     *
     * @param moduleMin: number min of bits to generate
     * @param moduleMax: number max of bits to generate
     * @return new prime number
     */
    public BigInteger genPrime(int moduleMin, int moduleMax) {
        BigInteger newPrime = this.getRandomBigInteger(moduleMin, moduleMax);
        while (!this.isFirstPrime(newPrime, Constant.NB_PRIME_TEST)) {
            newPrime = this.getRandomBigInteger(moduleMin, moduleMax);
        }
        return newPrime;
    }

    /**
     * Method to check several times if the
     * number is prime calling isProbablePrime
     *
     * @param newPrime: number generated
     * @param nbPrimeTest: number of times to check
     * @return true if this BigInteger is probably prime
     * checking nbPrimeTest times
     */
    public boolean isFirstPrime(BigInteger newPrime, int nbPrimeTest) {
        while (nbPrimeTest > 0) {
            if (!this.isProbablePrime(newPrime)) {
                return false;
            }
            nbPrimeTest--;
        }
        return true;
    }

    /**
     * Method to check primality
     *
     * @param newPrime: number generated
     * @return true if this BigInteger is probably prime,
     * false if it's definitely composite
     */
    public boolean isProbablePrime(BigInteger newPrime) {
        BigInteger nMinusOne = newPrime.subtract(BigInteger.ONE);
        BigInteger m = nMinusOne;
        int k = 0;

        while (m.mod(Constant.TWO).equals(BigInteger.ZERO)) {
            m = m.divide(Constant.TWO);
            k++;
        }

        BigInteger a = this.getRandomBigIntegerMaxRange(nMinusOne);
        BigInteger b = a.modPow(m, newPrime);

        if (b.mod(newPrime).equals((BigInteger.ONE).mod(newPrime))) {
            return true;
        } else {
            for (int i = 0; i < k; i++) {
                if (b.mod(newPrime).equals((Constant.MINUS_ONE).mod(newPrime))) {
                    return true;
                } else {
                    b = b.modPow(Constant.TWO, newPrime);
                }
            }
        }

        return false;
    }

    /**
     * Method to generate a number with max
     * as upper limit
     *
     * @param max: upper limit
     * @return BigInteger
     */
    public BigInteger getRandomBigIntegerMaxRange(BigInteger max) {
        BigInteger bigInteger;
        do {
            bigInteger = new BigInteger(max.bitLength(), this.random);
        } while(bigInteger.compareTo(max) > 0);

        return bigInteger;
    }

    /**
     * Method to generate a number between
     * moduleMin bits and moduleMax bits
     *
     * @param moduleMin: number min of bits
     * @param moduleMax: number max of bits
     * @return BigInteger
     */
    public BigInteger getRandomBigInteger(int moduleMin, int moduleMax) {
        int nbBits = this.random.nextInt((moduleMax - moduleMin) + 1) + moduleMin;
        return new BigInteger(nbBits, this.random);
    }
}
