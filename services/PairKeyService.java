package services;

import models.PublicKey;
import models.SecretKey;

import java.io.*;
import java.math.BigInteger;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class PairKeyService {

    private PublicKey pk;
    private SecretKey sk;

    /**
     * Constructor PairKeyService
     */
    public PairKeyService() {}

    /**
     * Constructor PairKeyService
     *
     * @param pk: public key
     * @param sk: secret key
     */
    public PairKeyService(PublicKey pk, SecretKey sk) {
        this.pk = pk;
        this.sk = sk;
    }

    /**
     * Method to display
     */
    public void display() {
        this.pk.display();
        this.sk.display();
    }

    /**
     * Method to transform the .pub
     * file into PublicKey
     *
     * @param name: file name for public key
     */
    public void pubFileToPublicKey(String name) {
        this.pk = new PublicKey();
        String line = null;
        try {
            FileReader fileReader = new FileReader(name + ".pub");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            int i = 0;
            while((line = bufferedReader.readLine()) != null) {
                if (i != 0) {
                    String key = line.split("-")[0];
                    String value = line.split("-")[1];
                    switch (key) {
                        case "t":
                            this.pk.setModule(Integer.parseInt(value));
                            break;
                        case "b":
                            this.pk.setB(new BigInteger(value));
                            break;
                        case "n":
                            this.pk.setN(new BigInteger(value));
                            break;
                    }
                }
                ++i;
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to transform the .priv
     * file into SecretKey
     *
     * @param name: file name for secret key
     */
    public void privFileToSecretKey(String name) {
        this.sk = new SecretKey();
        String line = null;
        try {
            FileReader fileReader = new FileReader(name + ".priv");
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            int i = 0;
            while((line = bufferedReader.readLine()) != null) {
                if (i != 0) {
                    String key = line.split("-")[0];
                    String value = line.split("-")[1];
                    switch (key) {
                        case "t":
                            this.sk.setModule(Integer.parseInt(value));
                            break;
                        case "a":
                            this.sk.setA(new BigInteger(value));
                            break;
                        case "p":
                            this.sk.setP(new BigInteger(value));
                            break;
                        case "q":
                            this.sk.setQ(new BigInteger(value));
                            break;
                    }
                }
                ++i;
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to create .pub and
     * .priv files
     *
     * @param name: file name for secret
     *            and private keys
     */
    public void toFile(String name) {
        this.createPrivFile(name);
        this.createPubFile(name);
    }

    /**
     * Method to create a file with
     * the private key attributes
     *
     * @param name: name file to private key
     */
    private void createPrivFile(String name) {
        File privFile = new File(name + ".priv");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(privFile, "UTF-8");
            writer.print("");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println("Secret key: ");
        writer.println("t-" + this.sk.getModule());
        writer.println("a-" + this.sk.getA());
        writer.println("p-" + this.sk.getP());
        writer.println("q-" + this.sk.getQ());
        writer.close();
    }

    /**
     * Method to create a file with
     * the public key attributes
     *
     * @param name: name file to public key
     */
    private void createPubFile(String name) {
        File pubFile = new File(name + ".pub");
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(pubFile, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.println("Public key: ");
        writer.println("t-" + this.pk.getModule());
        writer.println("b-" + this.pk.getB());
        writer.println("n-" + this.pk.getN());
        writer.close();
    }

    /**
     * Method to verify elements in keys
     * Rules: a*b = 1 [phiN]
     *
     * @return true if a*b = 1 [phiN]
     */
    public boolean verifyKey() {
        BigInteger phiN = this.sk.getPhiN();
        return (this.pk.getB().multiply(this.sk.getA()).mod(phiN)).equals(BigInteger.ONE) ||
                (this.pk.getB().multiply(this.sk.getA())).equals(BigInteger.ONE);
    }

    public PublicKey getPk() {
        return pk;
    }

    public SecretKey getSk() {
        return sk;
    }
}
