package services;

import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class ModularService {

    /**
     * Method to get the inverse of a modulo b
     *
     * @param a: a to get inverse
     * @param b: b modulo
     * @return inverse of a modulo b
     */
    public BigInteger modInverse(BigInteger a, BigInteger b) {
        ArrayList<BigInteger> rest = new ArrayList<>();
        ArrayList<BigInteger> inverse_a = new ArrayList<>();
        ArrayList<BigInteger> inverse_b = new ArrayList<>();
        // Initialization rest
        rest.add(a);
        rest.add(b);

        // Initialization a
        inverse_a.add(BigInteger.ONE);
        inverse_a.add(BigInteger.ZERO);

        // Initialization b
        inverse_b.add(BigInteger.ZERO);
        inverse_b.add(BigInteger.ONE);

        BigInteger quotient = rest.get(0).divide(rest.get(1));
        rest.add(rest.get(0).subtract(quotient.multiply(rest.get(1))));
        int i = 2;

        while (rest.get(i).compareTo(BigInteger.ZERO) > 0) {
            inverse_a.add(i, inverse_a.get(i-2).subtract(quotient.multiply(inverse_a.get(i-1))));
            inverse_b.add(i, inverse_b.get(i-2).subtract(quotient.multiply(inverse_b.get(i-1))));
            quotient = rest.get(i-1).divide(rest.get(i));
            i++;
            rest.add(i, rest.get(i-2).subtract(quotient.multiply(rest.get(i-1))));
        }

        return inverse_a.get(i-1).mod(b);
    }
}
