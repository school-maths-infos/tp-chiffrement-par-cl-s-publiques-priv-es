package controllers;

import models.Cipher;
import models.Message;
import models.Sign;
import services.GeneratorService;
import services.PairKeyService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Rsa {

    private PairKeyService pairKeyService;
    private GeneratorService generatorService;

    /**
     * Constructor Rsa
     */
    public Rsa() {
        this.generatorService = new GeneratorService();
        this.pairKeyService = new PairKeyService();
    }

    /**
     * Constructor Rsa
     * @param publicName: name file for public key
     * @param secretName: name file for secret key
     */
    public Rsa(String publicName, String secretName) {
        this.generatorService = new GeneratorService();
        this.pairKeyService = new PairKeyService();
        this.pairKeyService.pubFileToPublicKey(publicName);
        this.pairKeyService.privFileToSecretKey(secretName);
    }

    /**
     * Method to get the size of module
     * @return The size of module
     */
    public int getModule() {
        return this.pairKeyService.getPk().getModule();
    }

    /**
     * Method to encrypt a message
     * using public key
     * Rules: c = m^b [n]
     *
     * @param message: message to encrypt
     * @return message encrypted represented by a BigInteger
     */
    public BigInteger encrypt(Message message) {
        // Public key attributes
        BigInteger b = this.pairKeyService.getPk().getB();
        BigInteger n = this.pairKeyService.getPk().getN();

        return message.sequenceToBigInteger(message.getBytes()).modPow(b, n);
    }

    /**
     * Method to decrypt a cipher
     * using secret key
     * Rules: m = c^a [n]
     *
     * @param cipher: cipher to decrypt
     * @return: message decrypted represented by a String
     */
    public Message decrypt(Cipher cipher) {
        // Secret key attributes
        BigInteger a = this.pairKeyService.getSk().getA();
        BigInteger n = this.pairKeyService.getSk().getN();

        // Decrypt with ECB mode encryption
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            for (int i = 0; i < cipher.getSegmentedCipher().size(); i++) {
                BigInteger mSequence = cipher.getSegmentedCipher().get(i).modPow(a, n);
                outputStream.write(mSequence.toByteArray());
            }
            return new Message(outputStream.toByteArray());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            try {
                outputStream.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    /**
     * Method to sign a message
     * using the secret key
     * Rules: s = m^a [n]
     *
     * @param message: message to sign
     * @return message signed represented by a BigInteger
     */
    public Sign sign(Message message) {
        // Secret key attributes
        BigInteger a = this.pairKeyService.getSk().getA();
        BigInteger n = this.pairKeyService.getSk().getN();

        BigInteger s = message.digestToBigInteger().modPow(a, n);

        return new Sign(s);
    }

    /**
     * Method to verify a signature
     * using the public key
     * Rules: m = s^b [n]
     *
     * @param message: message to check signature
     * @param sign: signature to verify
     * @return true if original message string and
     * unsigned message string are equals
     */
    public boolean verify(Message message, Sign sign) {
        // Public key attributes
        BigInteger b = this.pairKeyService.getPk().getB();
        BigInteger n = this.pairKeyService.getPk().getN();

        BigInteger m = sign.getSignText().modPow(b, n);

        return m.equals(message.digestToBigInteger().mod(n));
    }

    /**
     * Method to generate public key
     * and secret key saving in files
     * .priv and .pub
     *
     * @param name: the name of files
     * @param module: the number of bits
     * @param useCustomMethods: to know if we use our custom methods or
     *                        BigInteger methods
     */
    public void genKey(String name, int module, boolean useCustomMethods) {
        this.pairKeyService = this.generatorService.genKey(module, useCustomMethods);

        if (this.pairKeyService.verifyKey()) {
            // Write in file
            this.pairKeyService.toFile(name);
            this.displayKeys();
        } else {
            System.out.println("Not ok.");
        }
    }

    public void displayKeys() {
        this.pairKeyService.display();
    }

    /**
     * Method to initialize service with key set
     * @param publicName
     * @param secretName
     */
    public void initializePairKeyService(String publicName, String secretName) {
        this.pairKeyService.pubFileToPublicKey(publicName);
        this.pairKeyService.privFileToSecretKey(secretName);
    }
}
