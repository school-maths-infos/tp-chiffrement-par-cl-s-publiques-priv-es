import controllers.Rsa;
import models.Message;
import models.Sign;

import java.io.File;
import java.util.Scanner;

/**
 * Info006 - TP RSA
 * de Roland Céline - Leclaire Juliana
 */
public class Signe {
    public static Rsa rsa;

    public static void main(String[] args) {
        if (args.length == 1) {
            String name = args[0];
            File pubFile = new File(name + ".pub");
            File privFile = new File(name + ".priv");
            if (pubFile.exists() && !pubFile.isDirectory() &&
                    privFile.exists() && !privFile.isDirectory()) {
                rsa = new Rsa(name, name);
                Scanner scanner = new Scanner(System.in);
                String messageString = "";
                String s = scanner.nextLine();
                while (scanner.hasNext() && !s.equals("")) {
                    messageString += s;
                    s = scanner.nextLine();
                }
                messageString += s;
                Message message = new Message(messageString);
                Sign sign = rsa.sign(message);
                System.out.println(sign.getSignText());
            } else {
                System.out.println("The file doesn't exists.");
            }
        } else {
            System.out.println("Usage : " +
                    "    java Signe [nom]   : signer le message sur l'entrée standard"
            );
        }
    }
}
